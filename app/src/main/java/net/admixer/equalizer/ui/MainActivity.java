package net.admixer.equalizer.ui;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.DynamicsProcessing;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.admixer.equalizer.Const;
import net.admixer.equalizer.R;
import net.admixer.equalizer.services.EqualizerService;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUI();
//        stopLocalAudio();

        SharedPreferences preferences = getSharedPreferences(Const.EQUALIZER_PREFERENCES, MODE_PRIVATE);
        boolean equalizerTurnedOn = preferences.getBoolean(Const.EQUALIZER_TURNED_ON, true);

        if(equalizerTurnedOn) {
            startEqualizerService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateTurnOnButton();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocalAudio();
    }

    private void setupUI(){

        Button activateEqualizer = findViewById(R.id.activate_equalizer);
        activateEqualizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEqualizerServiceRunning()) {
                    stopEqualizerService();
                } else {
                    startEqualizerService();
                }
            }
        });

    }

    private void startEqualizerService() {
        SharedPreferences preferences = getSharedPreferences(Const.EQUALIZER_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putBoolean(Const.EQUALIZER_TURNED_ON, true).apply();

        Intent service = new Intent(getApplicationContext(), EqualizerService.class);
        getApplicationContext().startForegroundService(service);

        Button activateEqualizer = findViewById(R.id.activate_equalizer);
        activateEqualizer.setText(R.string.turn_off);
    }

    private void stopEqualizerService(){
        SharedPreferences preferences = getSharedPreferences(Const.EQUALIZER_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putBoolean(Const.EQUALIZER_TURNED_ON, false).apply();

        Intent intent = new Intent(this, EqualizerService.class);
        intent.setAction(Const.ACTION_STOP_SERVICE);
        startForegroundService(intent);

        Button activateEqualizer = findViewById(R.id.activate_equalizer);
        activateEqualizer.setText(R.string.turn_on);
    }

    private void updateTurnOnButton(){
        Button turnOn = findViewById(R.id.activate_equalizer);
        if(isEqualizerServiceRunning()) {
            turnOn.setText(R.string.turn_off);
        } else {
            turnOn.setText(R.string.turn_on);
        }
    }

    private boolean isEqualizerServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (EqualizerService.class.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void startLocalAudio(){
        AudioManager am = (AudioManager) getApplicationContext().getSystemService(AUDIO_SERVICE);
        if(am != null) {
            int sessionId = am.generateAudioSessionId();
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setLegacyStreamType(AudioManager.STREAM_MUSIC).build();

            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.music, attributes, sessionId);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        }

    }

    private void stopLocalAudio(){
        if(mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }
}