package net.admixer.equalizer;

public interface Const {

    String NOTIFICATION_ACTION = "notification_action";
    String AUDIO_SESSION_ID = "audio_session_id";
    String PACKAGE_NAME = "package_name";

    String EQUALIZER_PREFERENCES = "equalizer_preferences";
    String EQUALIZER_TURNED_ON = "equalizer_turned_on";

    int STOP_SERVICE = 0;
    int PAUSE_EQUALIZER = 1;
    int RESUME_EQUALIZER = 2;


    String ACTION_STOP_SERVICE = "action_stop_service";
    String ACTION_OPEN_AUDIO_SESSION = "open_audio_session";
    String ACTION_CLOSE_AUDIO_SESSION = "close_audio_session";
    String ACTION_PAUSE_EQUALIZER = "pause_equalizer";
    String ACTION_RESUME_EQUALIZER = "resume_equalizer";

}
