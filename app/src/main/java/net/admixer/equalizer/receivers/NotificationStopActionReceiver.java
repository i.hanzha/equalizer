package net.admixer.equalizer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.admixer.equalizer.Const;
import net.admixer.equalizer.services.EqualizerService;

public class NotificationStopActionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.hasExtra(Const.NOTIFICATION_ACTION)) {
            int action = intent.getIntExtra(Const.NOTIFICATION_ACTION, -1);
            if(action == Const.STOP_SERVICE) {
                Intent service = new Intent(context, EqualizerService.class);
                service.setAction(Const.ACTION_STOP_SERVICE);
                context.startForegroundService(service);
            }
        }
    }
}
