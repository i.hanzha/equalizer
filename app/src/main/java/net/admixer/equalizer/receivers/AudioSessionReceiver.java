package net.admixer.equalizer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.audiofx.AudioEffect;
import android.util.Log;

import net.admixer.equalizer.Const;
import net.admixer.equalizer.services.EqualizerService;

public class AudioSessionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction() != null) {
            switch (intent.getAction()) {
                case "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION": {
                    int id = intent.getIntExtra(AudioEffect.EXTRA_AUDIO_SESSION, -1);
                    String packageName = intent.getStringExtra(AudioEffect.EXTRA_PACKAGE_NAME);
                    Log.d("MyCustomLog", String.format("open audio session id %s", id));

                    SharedPreferences preferences = context.getSharedPreferences(Const.EQUALIZER_PREFERENCES, Context.MODE_PRIVATE);
                    boolean equalizerTurnedOn = preferences.getBoolean(Const.EQUALIZER_TURNED_ON, true);

                    if(equalizerTurnedOn) {
                        Intent openIntent = new Intent(context, EqualizerService.class);
                        openIntent.setAction(Const.ACTION_OPEN_AUDIO_SESSION);
                        openIntent.putExtra(Const.AUDIO_SESSION_ID, id);
                        openIntent.putExtra(Const.PACKAGE_NAME, packageName);
                        context.startForegroundService(openIntent);
                    }
                    break;
                }
                case "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION":
                    Log.d("MyCustomLog", "close audio session");

                    SharedPreferences preferences = context.getSharedPreferences(Const.EQUALIZER_PREFERENCES, Context.MODE_PRIVATE);
                    boolean equalizerTurnedOn = preferences.getBoolean(Const.EQUALIZER_TURNED_ON, true);

                    if(equalizerTurnedOn) {
                        Intent closeIntent = new Intent(context, EqualizerService.class);
                        closeIntent.setAction(Const.ACTION_CLOSE_AUDIO_SESSION);
                        context.startForegroundService(closeIntent);
                    }
                    break;
            }
        }
    }

}
