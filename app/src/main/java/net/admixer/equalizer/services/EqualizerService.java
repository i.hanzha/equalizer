package net.admixer.equalizer.services;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import net.admixer.equalizer.Const;
import net.admixer.equalizer.equalizer.EqualizerManager;
import net.admixer.equalizer.R;
import net.admixer.equalizer.receivers.NotificationPauseActionReceiver;
import net.admixer.equalizer.receivers.NotificationResumeActionReceiver;
import net.admixer.equalizer.receivers.NotificationStopActionReceiver;
import net.admixer.equalizer.ui.MainActivity;

public class EqualizerService extends Service {

    private Handler timerHandler;
    private AudioManager audioManager;
    private int lastAudioSessionId = -1;
    private int lastVolumeLevel = 0;

    private EqualizerManager equalizerManager;
    private boolean isActive = true;
    private String audioAppName = "";

    public static final String CHANNEL_ID = "EqualizerServiceChannel";
    public static final int NOTIFICATION_ID = 3123;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d("MyCustomLog", String.format("onStartCommand %s", intent.getAction()));

        if(intent.getAction() != null) {
            switch (intent.getAction()) {
                case Const.ACTION_OPEN_AUDIO_SESSION:
                    openAudioSession(intent);
                    return START_NOT_STICKY;

                case Const.ACTION_CLOSE_AUDIO_SESSION:
                    closeAudioSession();
                    return START_NOT_STICKY;

                case Const.ACTION_STOP_SERVICE:
                    stopEqualizerService();
                    return START_NOT_STICKY;

                case Const.ACTION_PAUSE_EQUALIZER:
                    pauseEqualizer();
                    return START_NOT_STICKY;

                case Const.ACTION_RESUME_EQUALIZER:
                    resumeEqualizer();
                    return START_NOT_STICKY;

                default:
                    startEqualizerService();
                    return START_NOT_STICKY;
            }
        } else {
            startEqualizerService();
            return START_NOT_STICKY;
        }
    }

    private void openAudioSession(Intent intent){
        int audioSessionId = intent.getIntExtra(Const.AUDIO_SESSION_ID, -1);
        String packageName = intent.getStringExtra(Const.PACKAGE_NAME);

        if(equalizerManager == null) {
            equalizerManager = new EqualizerManager();
        }

        // Activate equalizer if active and headset or bluetooth
        if((lastAudioSessionId == -1 || lastAudioSessionId != audioSessionId)) {
            equalizerManager.activateEqualizer(audioSessionId);
            if(isHeadsetOrBluetooth() && isActive) {
                equalizerManager.applyEqualizerToVolumeLevel(getVolumeLevel());
            }
            lastAudioSessionId = audioSessionId;
        }

        // Start timer to check volume level
        if(timerHandler == null) {
            timerHandler = new Handler(Looper.getMainLooper());
        }
        startTimer(timerHandler);

        // Show notification
        switch (packageName) {
            case "com.google.android.apps.youtube.music":
                audioAppName = "YouTube Music";
                break;
            case "com.spotify.music":
                audioAppName = "Spotify";
                break;
        }
        startForeground(NOTIFICATION_ID, createNotification());
    }

    private void closeAudioSession(){
        lastAudioSessionId = -1;
        audioAppName = "";
        stopTimer();

        startForeground(NOTIFICATION_ID, createNotification());
    }

    private void startEqualizerService(){
        createNotificationChannel();
        Notification notification = createNotification();
        Log.d("MyCustomLog", "startForeground");
        startForeground(NOTIFICATION_ID, notification);
    }

    private void stopEqualizerService(){
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.cancel(NOTIFICATION_ID);
        stopForeground(true);
        stopSelf();

        SharedPreferences preferences = getSharedPreferences(Const.EQUALIZER_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putBoolean(Const.EQUALIZER_TURNED_ON, false).apply();
    }

    private void pauseEqualizer(){
        if(equalizerManager != null) {
            equalizerManager.disableEqualizer();
        }

        isActive = false;
        startForeground(NOTIFICATION_ID, createNotification());
    }

    private void resumeEqualizer(){
        if(equalizerManager != null) {
            equalizerManager.enabledEqualizer();
            equalizerManager.applyEqualizerToVolumeLevel(getVolumeLevel());
        }

        isActive = true;
        startForeground(NOTIFICATION_ID, createNotification());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyCustomLog", "onCreate");

        lastVolumeLevel = getVolumeLevel();
        equalizerManager = new EqualizerManager();
    }

    private void startTimer(Handler handler) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateEqualizerForVolumeLevel();
                startTimer(handler);
            }
        }, 1000);
    }

    private void stopTimer(){
        if(timerHandler != null) {
            timerHandler.removeCallbacksAndMessages(null);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyCustomLog", "onDestroy");

        stopTimer();
        if(equalizerManager != null) {
            equalizerManager.destroyEqualizer();
        }

    }

    private Notification createNotification(){
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, PendingIntent.FLAG_MUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, PendingIntent.FLAG_ONE_SHOT);
        }

        Intent stopServiceIntent = new Intent(this, NotificationStopActionReceiver.class);
        stopServiceIntent.putExtra(Const.NOTIFICATION_ACTION, Const.STOP_SERVICE);
        PendingIntent stopPendingIntent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            stopPendingIntent = PendingIntent.getBroadcast(this, 0,
                    stopServiceIntent, PendingIntent.FLAG_MUTABLE);
        } else {
            stopPendingIntent = PendingIntent.getBroadcast(this, 0,
                    stopServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        }

        String description = getString(R.string.notification_description);
        if(!"".equals(audioAppName)) {
            description = getString(R.string.you_listen, audioAppName);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(description)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .addAction(R.mipmap.ic_launcher, getString(R.string.stop), stopPendingIntent);

        if(lastAudioSessionId > 0) {
            Intent pauseEqualizerIntent = new Intent(this, NotificationPauseActionReceiver.class);
            pauseEqualizerIntent.putExtra(Const.NOTIFICATION_ACTION, Const.PAUSE_EQUALIZER);
            PendingIntent pausePendingIntent;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                pausePendingIntent = PendingIntent.getBroadcast(this, 0,
                        pauseEqualizerIntent, PendingIntent.FLAG_MUTABLE);
            } else {
                pausePendingIntent = PendingIntent.getBroadcast(this, 0,
                        pauseEqualizerIntent, PendingIntent.FLAG_ONE_SHOT);
            }

            Intent resumeEqualizerIntent = new Intent(this, NotificationResumeActionReceiver.class);
            resumeEqualizerIntent.putExtra(Const.NOTIFICATION_ACTION, Const.RESUME_EQUALIZER);
            PendingIntent resumePendingIntent;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                resumePendingIntent = PendingIntent.getBroadcast(this, 0,
                        resumeEqualizerIntent, PendingIntent.FLAG_MUTABLE);
            } else {
                resumePendingIntent = PendingIntent.getBroadcast(this, 0,
                        resumeEqualizerIntent, PendingIntent.FLAG_ONE_SHOT);
            }

            if (isActive) {
                builder.addAction(R.mipmap.ic_launcher, getString(R.string.pause), pausePendingIntent);
            } else {
                builder.addAction(R.mipmap.ic_launcher, getString(R.string.resume), resumePendingIntent);
            }
        }

        return builder.build();
    }

    private void createNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, "Equalizer Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    private int getVolumeLevel(){
        if(audioManager == null) {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    private void updateEqualizerForVolumeLevel() {
        if(equalizerManager == null) {
            equalizerManager = new EqualizerManager();
        }

        int currentVolumeLevel = getVolumeLevel();
        Log.d("MyCustomLog", String.format("volumeLevel %s | isHeadsetOn %s", currentVolumeLevel, audioManager.isWiredHeadsetOn()));

        if(isHeadsetOrBluetooth()) {
            if (isActive) {
                if(!equalizerManager.isEqualizerEnabled()) {
                    equalizerManager.enabledEqualizer();
                    equalizerManager.applyEqualizerToVolumeLevel(currentVolumeLevel);
                }
                if(currentVolumeLevel != lastVolumeLevel) {
                    equalizerManager.applyEqualizerToVolumeLevel(currentVolumeLevel);
                    lastVolumeLevel = currentVolumeLevel;
                }
            }
        } else {
            // disable equalizer for phone speaker
            equalizerManager.disableEqualizer();
        }
    }

    private boolean isHeadsetOrBluetooth(){
//        return audioManager.isWiredHeadsetOn() || audioManager.isBluetoothA2dpOn();
        return true;
    }

}
