package net.admixer.equalizer.equalizer;

import android.media.audiofx.DynamicsProcessing;
import android.util.Log;

public class EqualizerManager {

    private DynamicsProcessing dynamicsProcessing;
    // Relation between band gain and volume level
    private final IEqualizerParams equalizerParams;

    public EqualizerManager() {
        equalizerParams = new EqualizerParams();
    }

    public void activateEqualizer(int sessionId){
        Log.d("MyCustomLog", "activateEqualizer");
        DynamicsProcessing.Config.Builder builder = new DynamicsProcessing.Config.Builder(
                DynamicsProcessing.VARIANT_FAVOR_FREQUENCY_RESOLUTION, 1,
                true, equalizerParams.getBandNumber(),
                false, 0,
                false, 0,
                true
        );
        builder.setPreferredFrameDuration(10);

        DynamicsProcessing.Config config = builder.build();
        DynamicsProcessing.Eq eq = config.getChannelByChannelIndex(0).getPreEq();
        for(int i = 0;i < equalizerParams.getBandNumber();i++) {
            DynamicsProcessing.EqBand band = eq.getBand(i);
            band.setGain(0*1f);
            band.setCutoffFrequency(equalizerParams.getCutoffFrequencies()[i]);
            config.setPreEqBandAllChannelsTo(i, band);
            Log.d("MyCustomLog", String.format("band %s cutoffFrequency %s", i, band.getCutoffFrequency()));
        }

        dynamicsProcessing = new DynamicsProcessing(0, sessionId, config);
        dynamicsProcessing.setEnabled(true);
    }

    public void applyEqualizerToVolumeLevel(int volume){
        if(dynamicsProcessing != null && volume < equalizerParams.getBandGains()[0].length) {
            for (int i = 0; i < equalizerParams.getBandNumber(); i++) {
                DynamicsProcessing.EqBand band = dynamicsProcessing.getPreEqBandByChannelIndex(0, i);
                band.setGain(equalizerParams.getBandGains()[i][volume]);
                dynamicsProcessing.setPreEqBandAllChannelsTo(i, band);

                // Log only low bands
                if(i < 10) {
                    Log.d("MyCustomLog", String.format("band %s gain %s cutoffFrequency %s",
                            i, equalizerParams.getBandGains()[i][volume], band.getCutoffFrequency()));
                }
            }
        }
    }

    public void resetEqualizerToDefault(){
        if(dynamicsProcessing != null) {
            for(int i = 0;i < equalizerParams.getBandNumber();i++) {
                DynamicsProcessing.EqBand band = dynamicsProcessing.getPreEqBandByChannelIndex(0, i);
                band.setGain(0);
                dynamicsProcessing.setPreEqBandAllChannelsTo(i, band);
            }
        }
    }

    public void disableEqualizer(){
        if(dynamicsProcessing != null) {
            dynamicsProcessing.setEnabled(false);
        }
    }

    public void enabledEqualizer(){
        if(dynamicsProcessing != null) {
            dynamicsProcessing.setEnabled(true);
        }
    }

    public void destroyEqualizer(){
        if(dynamicsProcessing != null) {
            dynamicsProcessing.setEnabled(false);
            dynamicsProcessing = null;
        }
    }

    public boolean isEqualizerEnabled(){
        return dynamicsProcessing != null && dynamicsProcessing.getEnabled();
    }

}
