package net.admixer.equalizer.equalizer;

public interface IEqualizerParams {

    float[][] getBandGains();

    int getBandNumber();

    float[] getCutoffFrequencies();

}
